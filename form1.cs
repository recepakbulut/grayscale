using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4
{
    public partial class Form1 : Form
    {
        Bitmap kaynak, islem;
        public Form1()
        {
            InitializeComponent();
        }

        private void açToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult sonuc = openFileDialog1.ShowDialog();
            if (sonuc == DialogResult.OK)
            {
                kaynak = new Bitmap(openFileDialog1.FileName);
                kaynakBox.Image = kaynak;
            }

        }

        private void kaydetToolStripMenuItem_Click(object sender, EventArgs e)
        {
           

        }

        private void islemBox_Click(object sender, EventArgs e)
        {

        }

        private void bt709ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;
            int yuk = kaynak.Height;

            islem = new Bitmap(gen, yuk);

            for (int y = 0; y < yuk; y++)
            {
                for (int x = 0; x < gen; x++)
                {
                    Color renkliRenk = kaynak.GetPixel(x, y);
                    int gri = (int)(renkliRenk.R * 0.2125 + renkliRenk.G * 0.7154 + renkliRenk.B * 0.072);
                    Color griRenk = Color.FromArgb(gri, gri, gri);
                    islem.SetPixel(x, y, griRenk);
                }
            }

            islemBox.Image = islem;

        }

        private void acıklıkToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void lumaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;
            int yuk = kaynak.Height;

            islem = new Bitmap(gen, yuk);

            for (int y = 0; y < yuk; y++)
            {
                for (int x = 0; x < gen; x++)
                {
                    Color renkliRenk = kaynak.GetPixel(x, y);
                    int gri = (int)(renkliRenk.R * 0.3 + renkliRenk.G * 0.59 + renkliRenk.B * 0.11);
                    Color griRenk = Color.FromArgb(gri, gri, gri);
                    islem.SetPixel(x, y, griRenk);
                }
            }

            islemBox.Image = islem;
        }

        private void kanalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;
            int yuk = kaynak.Height;

            islem = new Bitmap(gen, yuk);

            for (int y = 0; y < yuk; y++)
            {
                for (int x = 0; x < gen; x++)
                {
                    Color renkliRenk = kaynak.GetPixel(x, y);
                    int gri = renkliRenk.B;
                    Color griRenk = Color.FromArgb(gri, gri, gri);
                    islem.SetPixel(x, y, griRenk);
                }
            }

            islemBox.Image = islem;
        }

                private void ortalamaToolStripMenuItem_Click(object sender, EventArgs e)
                {
                    int gen = kaynak.Width;
                    int yuk = kaynak.Height;

                    islem = new Bitmap(gen, yuk);

                    for (int y = 0; y < yuk; y++)
                    {
                        for (int x = 0; x < gen; x++)
                        {
                            Color renkliRenk = kaynak.GetPixel(x, y);
                            int gri = (renkliRenk.R + renkliRenk.G + renkliRenk.B) / 3;
                            Color griRenk = Color.FromArgb(gri, gri, gri);
                            islem.SetPixel(x, y, griRenk);
                        }
                    }

                    islemBox.Image = islem;

                }

            }
        }

